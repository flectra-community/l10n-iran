# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Iran - Country States",
    "category": "l10n/Technical",
    "version": "2.0.3.0.0",
    "author": "Fadoo, Odoo Community Association (OCA)",
    "maintainer": ["saeed-raesi"],
    "website": "https://gitlab.com/flectra-community/l10n-iran",
    "license": "AGPL-3",
    "summary": "Add Iran States and Cities",
    "depends": ["base", "base_address_city"],
    "data": [
        "data/res.country.state.csv",
        "data/res.city.csv",
        "data/res_country_data.xml",
    ],
}
