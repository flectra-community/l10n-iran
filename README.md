# Flectra Community / l10n-iran

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_ir_hr_contract](l10n_ir_hr_contract/) | 2.0.3.0.0| Iran Hr Contract
[l10n_ir_states](l10n_ir_states/) | 2.0.3.0.0| Add Iran States and Cities
[l10n_ir_base](l10n_ir_base/) | 2.0.3.0.0| Iran Base Calendar


